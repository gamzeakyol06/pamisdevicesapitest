package Test;

import Base.Base;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;


import static io.restassured.RestAssured.given;

public class POST_Device_Test extends Base {
    String token = doPostRequestAuthorizeValidateToken(LOGIN_PAGE_URL);
    public static HashMap map = new HashMap<>();

    @BeforeTest()
    public void BeforeMethod(){

        map.put("name","Test Device Created by Test Automation");
        map.put("isActive",true);
        map.put("WorkCenterID",80);
        map.put("deviceGUID", "3fa85f64-5717-4562-b3fc-2c963f66afa6");
        map.put("additionalInfo","info");
        System.out.println(map);

    }

    @Test (priority = 1,description = "200 Success")
    public void POST_Create_Success() throws InterruptedException, IOException {

        given().headers("Authorization","Bearer "+ token).
                contentType("application/json").
                body(map).
                when().
                post(DEVICE_PAGE_URL + "Device/Create").
                then().
                statusCode(200).log().all();
    }

    @Test (priority = 2, description = "400 Not Success (Empty)")
    public void POST_Create_Not_Success() throws InterruptedException {

        map.put("isActive",false);
        map.put("deviceGUID", null);
        map.put("name",null);
        map.put("additionalInfo",null);
        System.out.println(map);

        given().headers("Authorization","Bearer "+token).
                contentType("application/json").
                body(map).
                when().
                post(DEVICE_PAGE_URL + "Device/Create").
                then().
                statusCode(400).log().all();
    }
}

